<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMusicVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('music_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('artist', 128);
            $table->string('song', 128);
            $table->string('album', 128);
            $table->string('genre', 128);
            $table->string('language', 128);
            $table->string('rating', 128);
            $table->date('release_date');
            $table->string('song_file', 256);
            $table->string('album_art', 256)->nullable();
            $table->string('copyright_no', 128)->nullable();
            $table->string('country', 128)->nullable();
            $table->string('directors', 256)->nullable();
            $table->string('writers', 256)->nullable();
            $table->string('producers', 256)->nullable();
            $table->text('cast')->nullable();
            $table->string('preview_link', 512)->nullable();
            $table->string('image_link', 512)->nullable();
            $table->string('vendor_id', 512)->nullable();
            $table->string('alternate_titles', 512)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('music_videos');
    }
}
