<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAudiobooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audiobooks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->string('author', 100);
            $table->string('narrator', 100);
            $table->string('genre', 100);
            $table->string('language', 100);
            $table->date('release_date');
            $table->string('audio_file', 256);
            $table->integer('copyright_no')->nullable();
            $table->string('country', 100)->nullable();
            $table->integer('isbn')->nullable();
            $table->text('synopsis')->nullable();
            $table->string('publisher', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audiobooks');
    }
}
