<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMusicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('musics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('artist', 50);
            $table->string('title', 100);
            $table->string('album_name', 50);
            $table->string('genre', 100);
            $table->string('language', 100);
            $table->string('rating', 50);
            $table->date('release_date');
            $table->string('audio_file', 256);
            $table->string('album_art', 256)->nullable();
            $table->integer('copyright_no')->nullable();
            $table->string('country', 100)->nullable();
            $table->string('songwriters', 256)->nullable();
            $table->string('composers', 256)->nullable();
            $table->integer('isrc')->nullable();
            $table->integer('iswc')->nullable();
            $table->string('recording_label')->nullable();
            $table->string('publisher', 100)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('musics');
    }
}
