<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->text('synopsis');
            $table->string('genre', 100);
            $table->string('language', 100);
            $table->string('rating', 50);
            $table->date('release_date');
            $table->string('run_time');
            $table->string('movie_file', 256);
            $table->string('poster_art', 256)->nullable();
            $table->string('trailer_file', 256)->nullable();
            $table->integer('copyright_no')->nullable();
            $table->string('country', 100)->nullable();
            $table->string('directors', 256)->nullable();
            $table->string('writers', 256)->nullable();
            $table->string('producers', 256)->nullable();
            $table->text('cast')->nullable();
            $table->string('preview_link', 512)->nullable();
            $table->string('image_link', 512)->nullable();
            $table->string('vendor_id', 512)->nullable();
            $table->string('alternate_titles', 512)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
