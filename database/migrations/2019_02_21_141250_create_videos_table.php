<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('creator', 128);
            $table->string('title', 128);
            $table->text('description');
            $table->string('language', 128);
            $table->string('video_file', 256);
            $table->date('creation_date')->nullable();
            $table->string('copyright_no', 128)->nullable();
            $table->string('directors', 256)->nullable();
            $table->string('writers', 256)->nullable();
            $table->string('producers', 256)->nullable();
            $table->text('cast')->nullable();
            $table->string('preview_link', 512)->nullable();
            $table->string('image_link', 512)->nullable();
            $table->string('vendor_id', 512)->nullable();
            $table->string('alternate_titles', 512)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
