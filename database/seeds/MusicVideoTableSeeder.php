<?php

use Illuminate\Database\Seeder;

class MusicVideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\MusicVideo::class, 5)->create();
    }
}
