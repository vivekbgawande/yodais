<?php

use Illuminate\Database\Seeder;

class AudioBookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\AudioBook::class, 5)->create();
    }
}
