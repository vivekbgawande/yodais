<?php

use Illuminate\Database\Seeder;

class DigitalArtworkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\DigitalArtwork::class, 5)->create();
    }
}
