<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            MovieTableSeeder::class,
            VideoTableSeeder::class,
            MusicVideoTableSeeder::class,
            MusicTableSeeder::class,
            AudioBookTableSeeder::class,
            PhotoTableSeeder::class,
            DigitalArtworkTableSeeder::class,
        ]);
    }
}
