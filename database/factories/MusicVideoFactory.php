<?php

use Faker\Generator as Faker;

$factory->define(App\MusicVideo::class, function (Faker $faker) {
    return [
        'artist' => $faker->word,
        'song' => $faker->sentence,
        'album' => $faker->word,
        'genre' => $faker->word,
        'language' => $faker->word,
        'rating' => $faker->word,
        'release_date' => $faker->date,
        'song_file' => $faker->sentence,
        'album_art' => $faker->sentence,
        'copyright_no' => $faker->randomNumber(),
        'country' => $faker->word,
        'directors' => $faker->sentence,
        'writers' => $faker->sentence,
        'producers' => $faker->sentence,
        'cast' => $faker->sentence,
        'preview_link' => $faker->sentence,
        'image_link' => $faker->sentence,
        'vendor_id' => $faker->sentence,
        'alternate_titles' => $faker->sentence,
    ];
});
