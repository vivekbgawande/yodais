<?php

use Faker\Generator as Faker;

$factory->define(App\DigitalArtwork::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'description' => $faker->sentence,
        'creation_date' => $faker->date,
        'location' => $faker->word,
        'file' => $faker->sentence,
        'copyright_no' => $faker->randomNumber(),
    ];
});
