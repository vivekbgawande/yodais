<?php

use Faker\Generator as Faker;

$factory->define(App\Music::class, function (Faker $faker) {
    return [
        'artist' => $faker->word,
        'title' => $faker->sentence,
        'album_name' => $faker->word,
        'genre' => $faker->word,
        'language' => $faker->word,
        'rating' => $faker->word,
        'release_date' => $faker->date,
        'audio_file' => $faker->sentence,
        'album_art' => $faker->sentence,
        'copyright_no' => $faker->randomNumber(),
        'country' => $faker->word,
        'songwriters' => $faker->sentence,
        'composers' => $faker->sentence,
        'isrc' => $faker->randomNumber(),
        'iswc' => $faker->randomNumber(),
        'recording_label' => $faker->word,
        'publisher' => $faker->sentence,
    ];
});
