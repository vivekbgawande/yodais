<?php

use Faker\Generator as Faker;

$factory->define(App\Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'synopsis' => $faker->sentence,
        'genre' => $faker->word,
        'language' => $faker->word,
        'rating' => $faker->word,
        'release_date' => $faker->date,
        'run_time' => $faker->time($format = 'H:i'),
        'movie_file' => $faker->sentence,
        'poster_art' => $faker->sentence,
        'trailer_file' => $faker->sentence,
        'copyright_no' => $faker->randomNumber(),
        'country' => $faker->word,
        'directors' => $faker->sentence,
        'writers' => $faker->sentence,
        'producers' => $faker->sentence,
        'cast' => $faker->sentence,
        'preview_link' => $faker->sentence,
        'image_link' => $faker->sentence,
        'vendor_id' => $faker->sentence,
        'alternate_titles' => $faker->sentence,
    ];
});
