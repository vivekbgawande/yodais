<?php

use Faker\Generator as Faker;

$factory->define(App\Video::class, function (Faker $faker) {
    return [
        'creator' => $faker->word,
        'title' => $faker->sentence,
        'description' => $faker->sentence,
        'language' => $faker->word,
        'video_file' => $faker->sentence,
        'creation_date' => $faker->date,
        'copyright_no' => $faker->randomNumber(),
        'directors' => $faker->sentence,
        'writers' => $faker->sentence,
        'producers' => $faker->sentence,
        'cast' => $faker->sentence,
        'preview_link' => $faker->sentence,
        'image_link' => $faker->sentence,
        'vendor_id' => $faker->sentence,
        'alternate_titles' => $faker->sentence,
    ];
});
