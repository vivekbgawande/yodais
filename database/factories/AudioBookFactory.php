<?php

use Faker\Generator as Faker;

$factory->define(App\AudioBook::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'author' => $faker->word,
        'narrator' => $faker->word,
        'genre' => $faker->word,
        'language' => $faker->word,
        'release_date' => $faker->date,
        'audio_file' => $faker->sentence,
        'copyright_no' => $faker->randomNumber(),
        'isbn' => $faker->randomNumber(),
        'synopsis' => $faker->sentence,
        'publisher' => $faker->sentence,
    ];
});
