<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DigitalArtwork extends Model
{
    protected $guarded = [];
}
