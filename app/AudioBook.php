<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AudioBook extends Model
{
    protected $table = 'audiobooks';
    protected $guarded = [];
}
