<?php

namespace App\Http\Controllers;

use App\MusicVideo;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MusicVideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $musicVideos = MusicVideo::all();
        return view('admin.music-videos', compact('musicVideos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MusicVideo  $musicVideo
     * @return \Illuminate\Http\Response
     */
    public function show(MusicVideo $musicVideo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MusicVideo  $musicVideo
     * @return \Illuminate\Http\Response
     */
    public function edit(MusicVideo $musicVideo)
    {
        return view('admin.edit-music-video', compact('musicVideo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MusicVideo  $musicVideo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MusicVideo $musicVideo)
    {
        $validatedData = $request->validate([
            'artist' => 'string',
            'song' => 'string',
            'album' => 'string',
            'genre' => 'string',
            'language' => 'string',
            'rating' => 'string',
            'release_date' => 'date',
            'song_file' => 'nullable|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4',
            'album_art' => 'nullable|image',
            'copyright_no' => 'nullable|string',
            'country' => 'nullable|string',
            'directors' => 'nullable|string',
            'writers' => 'nullable|string',
            'producers' => 'nullable|string',
            'cast' => 'nullable|string',
            'preview_link' => 'nullable|string',
            'image_link' => 'nullable|string',
            'vendor_id' => 'nullable|string',
            'alternate_titles' => 'nullable|string',
        ]);

        if (request()->file('song_file')) {
            $path = request()->file('song_file')->store("public/music-videos/{$musicVideo->id}");

            $newPath = Storage::url($path);

            File::delete(public_path() . $musicVideo->song_file);

            $musicVideo->update(['song_file' => $newPath]);
        }

        if (request()->file('album_art')) {
            $albumPath = request()->file('album_art')->store("public/music-videos/{$musicVideo->id}");
            $newPath = Storage::url($albumPath);

            File::delete(public_path() . $musicVideo->album_art);

            $musicVideo->update(['album_art' => $newPath]);
        }

        unset($validatedData['song_file']);
        unset($validatedData['album_art']);

        $musicVideo->update($validatedData);
        return redirect('/admin/music-videos');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MusicVideo  $musicVideo
     * @return \Illuminate\Http\Response
     */
    public function destroy(MusicVideo $musicVideo)
    {
        File::delete(public_path() . $musicVideo->song_file);
        File::delete(public_path() . $musicVideo->album_art);

        if (file_exists(public_path() . '/storage/music-videos/' . $musicVideo->id)) {
            rmdir(public_path() . '/storage/music-videos/' . $musicVideo->id);
        }

        $musicVideo->delete();

        return redirect('/admin/music-videos');
    }
}
