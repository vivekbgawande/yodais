<?php

namespace App\Http\Controllers;

use App\AudioBook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;

class AudioBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $audiobooks = AudioBook::all();
        return view('admin.audiobooks', compact('audiobooks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AudioBook  $audioBook
     * @return \Illuminate\Http\Response
     */
    public function show(AudioBook $audioBook)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AudioBook  $audioBook
     * @return \Illuminate\Http\Response
     */
    public function edit(AudioBook $audioBook)
    {
        return view('admin.edit-audiobook', compact('audioBook'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AudioBook  $audioBook
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AudioBook $audioBook)
    {
        $validatedData = request()->validate([
            'title' => 'required|min:3',
            'author' => 'required|min:3',
            'narrator' => 'required|min:3',
            'genre' => 'required',
            'language' => 'required',
            'release_date' => 'required|date',
            'audio_file' => 'mimes:application/octet-stream,audio/mpeg,mpga,mp3,wav',
            'copyright_no' => 'nullable|numeric',
            'country' => 'nullable|string',
            'isbn' => 'nullable|numeric',
            'synopsis' => 'nullable|string',
            'publisher' => 'nullable|string'
        ]);

        if (request()->has('audio_file')) {
            $path = $request->file('audio_file')->store("public/audiobooks/{$audioBook->id}");
            $newPath = Storage::url($path);

            File::delete(public_path() . $audioBook->audio_file);
            $audioBook->update(['audio_file' => $newPath]);
        }

        unset($validatedData['audio_file']);

        $audioBook->update($validatedData);

        return redirect('/admin/audiobooks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AudioBook  $audioBook
     * @return \Illuminate\Http\Response
     */
    public function destroy(AudioBook $audioBook)
    {
        File::delete(public_path() . $audioBook->audio_file);
        rmdir(public_path() . '/storage/audiobooks/' . $audioBook->id);

        $audioBook->delete();
        return redirect('/admin/audiobooks/');
    }
}
