<?php

namespace App\Http\Controllers;

use App\Movie;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::all();
        return view('admin.movies', compact('movies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        return view('admin.edit-movie', compact('movie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        $validatedData = $request->validate([
            'title' => 'string',
            'synopsis' => 'string',
            'genre' => 'string',
            'language' => 'string',
            'rating' => 'string',
            'release_date' => 'date',
            'run_time' => 'string',
            'movie_file' => 'nullable|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4',
            'poster_art' => 'nullable|image',
            'trailer_file' => 'nullable|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4',
            'copyright_no' => 'nullable|string',
            'country' => 'nullable|string',
            'directors' => 'nullable|string',
            'writers' => 'nullable|string',
            'producers' => 'nullable|string',
            'cast' => 'nullable|string',
            'preview_link' => 'nullable|string',
            'image_link' => 'nullable|string',
            'vendor_id' => 'nullable|string',
            'alternate_titles' => 'nullable|string',
        ]);

        if (request()->file('movie_file')) {
            // path gives /public/movies/3/foo.mp4
            $path = request()->file('movie_file')->store("public/movies/{$movie->id}");

            // newPath makes it /storage/movies/3/foo.mp4
            $newPath = Storage::url($path);

            File::delete(public_path() . $movie->movie_file);

            $movie->update(['movie_file' => $newPath]);
        }

        if (request()->file('poster_art')) {
            $posterPath = request()->file('poster_art')->store("public/movies/{$movie->id}");
            $newPath = Storage::url($posterPath);

            File::delete(public_path() . $movie->poster_art);

            $movie->update(['poster_art' => $newPath]);
        }

        if (request()->file('trailer_file')) {
            $trailerPath = request()->file('trailer_file')->store("public/movies/{$movie->id}");
            $newPath = Storage::url($trailerPath);

            File::delete(public_path() . $movie->trailer_file);

            $movie->update(['trailer_file' => $newPath]);
        }

        unset($validatedData['movie_file']);
        unset($validatedData['poster_art']);
        unset($validatedData['trailer_file']);

        $movie->update($validatedData);

        return redirect('/admin/movies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        File::delete(public_path() . $movie->movie_file);
        File::delete(public_path() . $movie->poster_art);
        File::delete(public_path() . $movie->trailer_file);

        if (file_exists(public_path() . '/storage/movies/' . $movie->id)) {
            rmdir(public_path() . '/storage/movies/' . $movie->id);
        }

        $movie->delete();

        return redirect('/admin/movies');
    }
}
