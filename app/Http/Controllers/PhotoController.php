<?php

namespace App\Http\Controllers;

use File;
use App\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photos = Photo::all();
        return view('admin.photos', compact('photos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        return view('admin.edit-photo', compact('photo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
        $validatedData = request()->validate([
            'title' => 'string',
            'description' => 'required',
            'creation_date' => 'date',
            'location' => 'required',
            'file' => 'image|mimes:png,jpg,jpeg',
            'copyright_no' => 'string|nullable',
        ]);

        if (request()->file('file')) {
            $photoPath = request()->file('file')->store("public/photos/{$photo->id}");
            $newPath = Storage::url($photoPath);

            File::delete(public_path() . $photo->file);

            $photo->update(['file' => $newPath]);
        }

        unset($validatedData['file']);

        $photo->update($validatedData);

        return redirect('/admin/photos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        File::delete(public_path() . $photo->file);

        if (file_exists(public_path() . '/storage/photos/' . $photo->id)) {
            rmdir(public_path() . '/storage/photos/' . $photo->id);
        }

        $photo->delete();
        return redirect('/admin/photos');
    }
}
