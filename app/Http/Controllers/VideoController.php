<?php

namespace App\Http\Controllers;

use File;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::all();
        return view('admin.videos', compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show(Video $video)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        return view('admin.edit-video', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Video $video)
    {
        $validatedData = $request->validate([
            'creator' => 'string',
            'title' => 'string',
            'description' => 'string',
            'language' => 'string',
            'video_file' => 'nullable|mimetypes:video/avi,video/mpeg,video/quicktime,video/mp4',
            'creation_date' => 'nullable|date',
            'copyright_no' => 'nullable|string',
            'directors' => 'nullable|string',
            'writers' => 'nullable|string',
            'producers' => 'nullable|string',
            'cast' => 'nullable|string',
            'preview_link' => 'nullable|string',
            'image_link' => 'nullable|string',
            'vendor_id' => 'nullable|string',
            'alternate_titles' => 'nullable|string',
        ]);


        if (request()->file('video_file')) {
            $path = request()->file('video_file')->store("public/videos/{$video->id}");

            $newPath = Storage::url($path);

            File::delete(public_path() . $video->video_file);

            $video->update(['video_file' => $newPath]);
        }

        unset($validatedData['video_file']);

        $video->update($validatedData);

        return redirect('/admin/videos');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        File::delete(public_path() . $video->video_file);
        
        if (file_exists(public_path() . '/storage/videos/' . $video->id)) {
            rmdir(public_path() . '/storage/videos/' . $video->id);
        }

        $video->delete();

        return redirect('/admin/videos');
    }
}
