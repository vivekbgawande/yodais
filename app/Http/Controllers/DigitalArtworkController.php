<?php

namespace App\Http\Controllers;

use File;
use App\DigitalArtwork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DigitalArtworkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $digital_artworks = DigitalArtwork::all();
        return view('admin.digital-artworks', compact('digital_artworks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DigitalArtwork  $digitalArtwork
     * @return \Illuminate\Http\Response
     */
    public function show(DigitalArtwork $digitalArtwork)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DigitalArtwork  $digitalArtwork
     * @return \Illuminate\Http\Response
     */
    public function edit(DigitalArtwork $digitalArtwork)
    {
        return view('admin.edit-digital-artwork', compact('digitalArtwork'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DigitalArtwork  $digitalArtwork
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DigitalArtwork $digitalArtwork)
    {
        $validatedData = request()->validate([
            'title' => 'string',
            'description' => 'required',
            'creation_date' => 'date',
            'location' => 'required',
            'file' => 'image|mimes:png,jpg,jpeg',
            'copyright_no' => 'string|nullable',
        ]);

        if (request()->file('file')) {
            $digitalArtworkPath = request()->file('file')->store("public/digital-artworks/{$digitalArtwork->id}");
            $newPath = Storage::url($digitalArtworkPath);

            File::delete(public_path() . $digitalArtwork->file);

            $digitalArtwork->update(['file' => $newPath]);
        }

        unset($validatedData['file']);

        $digitalArtwork->update($validatedData);

        return redirect('/admin/digital-artworks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DigitalArtwork  $digitalArtwork
     * @return \Illuminate\Http\Response
     */
    public function destroy(DigitalArtwork $digitalArtwork)
    {
        $digitalArtwork->delete();
        return redirect('/admin/digital-artworks/');
    }
}
