<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;

class BannerController extends Controller
{
    public function index()
    {
        return view('admin.homepagebanners');
    }
    public function store()
    {
        request()->validate([
            'movies_image' => 'image|mimes:jpg,png,jpeg',
            'photos_image' => 'image|mimes:jpg,png,jpeg',
            'artworks_image' => 'image|mimes:jpg,png,jpeg',
            'musics_image' => 'image|mimes:jpg,png,jpeg',
            'blogs_image' => 'image|mimes:jpg,png,jpeg',
            'subscribed_image' => 'image|mimes:jpg,png,jpeg',
            
        ]);

        if(request()->has('movies_image')){
            File::delete('storage/banners/movies.jpg');
            $path = request()->file('movies_image')->storeAs('public/banners', 'movies.jpg');
        }

        if(request()->has('photos_image')){
            File::delete('storage/banners/photos.jpg');
            $path = request()->file('photos_image')->storeAs('public/banners', 'photos.jpg');
        }

        if(request()->has('artworks_image')){
            File::delete('storage/banners/digital-artworks.jpg');
            $path = request()->file('artworks_image')->storeAs('public/banners', 'digital-artworks.jpg');
        }

        if(request()->has('musics_image')){
            File::delete('storage/banners/musics.jpg');
            $path = request()->file('musics_image')->storeAs('public/banners', 'musics.jpg');
        }

        if(request()->has('blogs_image')){
            File::delete('storage/banners/blogs.jpg');
            $path = request()->file('blogs_image')->storeAs('public/banners', 'blogs.jpg');
        }

        if(request()->has('subscribed_image')){
            File::delete('storage/banners/subscribed.jpg');
            $path = request()->file('subscribed_image')->storeAs('public/banners', 'subscribed.jpg');
        }

        return redirect('/admin/homepage');
    }
}
