<?php

namespace App\Http\Controllers;

use File;
use App\Music;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MusicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $musics = Music::all();
        return view('admin.music', compact('musics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Music  $music
     * @return \Illuminate\Http\Response
     */
    public function show(Music $music)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Music  $music
     * @return \Illuminate\Http\Response
     */
    public function edit(Music $music)
    {
        return view('admin.edit-music', compact('music'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Music  $music
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Music $music)
    {
        $validatedData = request()->validate([
            'artist' => 'string',
            'title' => 'required|string',
            'album_name' => 'required|string',
            'genre' => 'required',
            'language' => 'required',
            'rating' => 'required|string',
            'release_date' => 'required|date',
            'audio_file' => 'mimes:application/octet-stream,audio/mpeg,mpga,mp3,wav',
            'album_art' => 'nullable|image',
            'copyright_no' => 'nullable|numeric',
            'country' => 'nullable|alpha',
            'songwriters' => 'nullable|string',
            'composers' => 'nullable|string',
            'isrc' => 'nullable|numeric',
            'iswc' => 'nullable|numeric',
            'recording_label' => 'nullable|string',
            'publisher' => 'nullable|string',
        ]);

        if (request()->has('audio_file')) {
            $path = $request->file('audio_file')->store("public/music/{$music->id}");
            $newPath = Storage::url($path);

            File::delete(public_path() . $music->audio_file);
            $music->update(['audio_file' => $newPath]);
        }

        if (request()->has('album_art')) {
            $path = $request->file('album_art')->store("public/music/{$music->id}");
            $newPath = Storage::url($path);

            File::delete(public_path() . $music->album_art);
            $music->update(['album_art' => $newPath]);
        }

        unset($validatedData['audio_file']);
        unset($validatedData['album_art']);

        $music->update($validatedData);

        return redirect('/admin/music');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Music  $music
     * @return \Illuminate\Http\Response
     */
    public function destroy(Music $music)
    {
        File::delete(public_path() . $music->audio_file);
        File::delete(public_path() . $music->album_art);
        rmdir(public_path() . '/storage/music/' . $music->id);

        $music->delete();
        return redirect('/admin/music');
    }
}
