<?php

Route::get('/', function() {
    return view('user.index');
});

Route::get('/admin', "AdminController@index");

Route::get('/admin/movies', "MovieController@index");
Route::get('/admin/movies/{movie}/edit', "MovieController@edit");
Route::patch('/movies/{movie}', "MovieController@update");
Route::delete('/admin/movies/{movie}', "MovieController@destroy");


Route::get('/admin/music-videos', "MusicVideoController@index");
Route::get('/admin/music-videos/{musicVideo}/edit', "MusicVideoController@edit");
Route::patch('/music-videos/{musicVideo}', "MusicVideoController@update");
Route::delete('/admin/music-videos/{musicVideo}', "MusicVideoController@destroy");


Route::get('/admin/videos', "VideoController@index");  
Route::get('/admin/videos/{video}/edit', "VideoController@edit");
Route::patch('/admin/videos/{video}', "VideoController@update");
Route::delete('/admin/videos/{video}', "VideoController@destroy");


Route::get('/admin/music', "MusicController@index");
Route::get('/admin/music/{music}/edit', "MusicController@edit");
Route::patch('/admin/music/{music}', "MusicController@update");
Route::delete('/admin/music/{music}', "MusicController@destroy");


Route::get('/admin/audiobooks', "AudioBookController@index");
Route::get('/admin/audiobooks/{audioBook}/edit', "AudioBookController@edit");
Route::patch('/admin/audiobooks/{audioBook}', "AudioBookController@update");
Route::delete('/admin/audiobooks/{audioBook}', "AudioBookController@destroy");


Route::get('/admin/photos', "PhotoController@index");
Route::get('/admin/photos/{photo}/edit', "PhotoController@edit");
Route::patch('/admin/photos/{photo}', "PhotoController@update");
Route::delete('/admin/photos/{photo}', "PhotoController@destroy");


Route::get('/admin/digital-artworks', "DigitalArtworkController@index");
Route::get('/admin/digital-artworks/{digitalArtwork}/edit', "DigitalArtworkController@edit");
Route::patch('/admin/digital-artworks/{digitalArtwork}', "DigitalArtworkController@update");
Route::delete('/admin/digital-artworks/{digitalArtwork}', "DigitalArtworkController@destroy");


Route::get('/admin/users', function () {
    return view('admin.users');
});

Route::get('/admin/homepage', 'BannerController@index');
Route::post('/admin/homepage', 'BannerController@store');

Route::get('/user/profile', function () {
    return view('user.profile');
});

