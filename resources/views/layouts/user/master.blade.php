@include('layouts.user.navbar')

<div class="wrapper">
    @yield('content')
</div>

@include('layouts.user.footer')