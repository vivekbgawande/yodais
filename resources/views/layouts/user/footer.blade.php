<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h1 class="title">YODAIS</h1>
            </div>
            <div class="col-md-3">
                <ul class="nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                Movie/TV
              </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                Music Video
              </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                Video Clips
              </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                Blog
              </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <ul class="nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                Music
              </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                Audiobook
              </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                Photos
              </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                Digital Artwork
              </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3">
                <h3 class="title">Follow us:</h3>
                <div class="btn-wrapper profile">
                    <a target="_blank" href="#" class="btn btn-icon btn-neutral btn-round btn-simple" data-toggle="tooltip"
                        data-original-title="Follow us">
              <i class="fab fa-twitter"></i>
            </a>
                    <a target="_blank" href="#" class="btn btn-icon btn-neutral btn-round btn-simple" data-toggle="tooltip"
                        data-original-title="Like us">
              <i class="fab fa-facebook-square"></i>
            </a>
                    <a target="_blank" href="#" class="btn btn-icon btn-neutral  btn-round btn-simple" data-toggle="tooltip"
                        data-original-title="Follow us">
              <i class="fab fa-dribbble"></i>
            </a>
                </div>
            </div><hr>
            <div class="row" style="width: 100%">
                 <span style="color: #ffffff;font-size: 15px;margin: 0 auto;text-align: center;">&copy; 2019 Yodais. All Rights Reserved</span>
            </div>
        </div>
    </div>
</footer>
</div>
<!--   Core JS Files   -->
<script src="./assets/js/core/jquery.min.js" type="text/javascript"></script>
<script src="./assets/js/core/popper.min.js" type="text/javascript"></script>
<script src="./assets/js/core/bootstrap.min.js" type="text/javascript"></script>
<script src="./assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="./assets/js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="./assets/js/plugins/nouislider.min.js" type="text/javascript"></script>
<!-- Chart JS -->
<script src="./assets/js/plugins/chartjs.min.js"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="./assets/js/plugins/moment.min.js"></script>
<script src="./assets/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
<!-- Black Dashboard DEMO methods, don't include it in your project! -->
<script src="./assets/demo/demo.js"></script>
<!-- Control Center for Black UI Kit: parallax effects, scripts for the example pages etc -->
<script src="./assets/js/blk-design-system.min.js?v=1.0.0" type="text/javascript"></script>
<script>
    $(document).ready(function() {
    blackKit.initDatePicker();
    blackKit.initSliders();
  });

  function scrollToDownload() {

    if ($('.section-download').length != 0) {
      $("html, body").animate({
        scrollTop: $('.section-download').offset().top
      }, 1000);
    }
  }

</script>
</body>

</html>