<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="/admin">
            <i class="nav-icon icon-bell"></i> Dashboard
          </a>
      </li>
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
              <i class="nav-icon icon-speedometer"></i>Videos</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="/admin/movies">
                <i class="nav-icon icon-cursor"></i>Movie/TV</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/music-videos">
                <i class="nav-icon icon-cursor"></i>Music Video</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/videos">
                <i class="nav-icon icon-cursor"></i>Video Clip</a>
          </li>
        </ul>
      </li>

      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon icon-pie-chart"></i>Audio</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="/admin/music">
                <i class="nav-icon icon-cursor"></i>Music</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/audiobooks">
                <i class="nav-icon icon-cursor"></i>Audiobook</a>
          </li>
        </ul>
      </li>
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
            <i class="nav-icon icon-puzzle"></i>Images</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="/admin/photos">
                <i class="nav-icon icon-star"></i> Photo </a>
            <a class="nav-link" href="/admin/digital-artworks">
                <i class="nav-icon icon-star"></i> Digital Artwork</a>
          </li>
        </ul>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/admin/users">
          <i class="nav-icon icon-user"></i>Manage Users
        </a>
      </li> 
      <li class="nav-item">
        <a class="nav-link" href="/admin/homepage">
          <i class="nav-icon icon-star"></i>Homepage Banners 
        </a>
      </li>  
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>