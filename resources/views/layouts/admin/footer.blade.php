<footer class="app-footer">
    <div>
        <a href="">Admin Panel</a>
        <span>&copy; 2019 Yodais. All Rights Reserved</span>
    </div>
</footer>

<script src="/vendors/jquery/js/jquery.min.js"></script>
<script src="/vendors/popper.js/js/popper.min.js"></script>
<script src="/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="/vendors/@coreui/coreui/js/coreui.min.js"></script>
<script src="/vendors/@coreui/coreui-plugin-chartjs-custom-tooltips/js/custom-tooltips.min.js"></script>
</body>

</html>