@include('layouts.admin.header')

<div class="app-body">
    @include('layouts.admin.sidebar')
    <main class="main">
        <div class="container-fluid mt-3">
            <div class="animated fadeIn">
                @yield('content')
            </div>
        </div>
    </main>

</div>
    @include('layouts.admin.footer')