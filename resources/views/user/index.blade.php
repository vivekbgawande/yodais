@extends('layouts.user.master') 
@section('content')

<div class="page-header header-filter">
  <div class="squares square1"></div>
  <div class="squares square2"></div>
  <div class="squares square3"></div>
  <div class="squares square4"></div>
  <div class="squares square5"></div>
  <div class="squares square6"></div>
  <div class="squares square7"></div>
  <div class="container">
    <div class="content-center brand">
      <h1 class="h1-seo">YODAIS</h1>
      <h3>Welcome to the Content Network</h3>
    </div>
  </div>
</div>
<div class="main mb-5">
  {{--
  <div class="section section-basic" id="basic-elements">
    <div class="row col-lg-14 mt-4">
      <img src="/img/Yodais---The-Content-Network.png">
      <!-- Cover Photo-->
    </div> --}}
    <div class="container">

      <div class="row mt-5">
        <div class="col-sm-6">
          <div class="hvrbox mt-4">
            <div class="container">
              <img class="hvrbox-layer_bottom" src="/storage/banners/movies.jpg" style="width:100%;">
              <div class="text-block">
                <h4>Movie/TV</h4>
              </div>
            </div>
            <div class="hvrbox-layer_top">
              <div class="hvrbox-text"><button>See More</button></div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="hvrbox mt-4">
            <div class="container">
              <img class="hvrbox-layer_bottom" src="/storage/banners/subscribed.jpg" style="width:100%;">
              <div class="text-block">
                <h4>Subscribed</h4>
              </div>
            </div>
            <div class="hvrbox-layer_top">
              <div class="hvrbox-text"><button>See More</button></div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="hvrbox mt-4">
            <div class="container">
              <img class="hvrbox-layer_bottom" src="/storage/banners/photos.jpg" style="width:100%;">
              <div class="text-block">
                <h4>Photos</h4>
              </div>
            </div>
            <div class="hvrbox-layer_top">
              <div class="hvrbox-text"><button>See More</button></div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="hvrbox mt-4">
            <div class="container">
              <img class="hvrbox-layer_bottom" src="/storage/banners/musics.jpg" style="width:100%;">
              <div class="text-block">
                <h4>Music</h4>
              </div>
            </div>
            <div class="hvrbox-layer_top">
              <div class="hvrbox-text"><button>See More</button></div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6">
          <div class="hvrbox mt-4">
            <div class="container">
              <img class="hvrbox-layer_bottom" src="/storage/banners/digital-artworks.jpg" style="width:100%;">
              <div class="text-block">
                <h4>Digital Artwork</h4>
              </div>
            </div>
            <div class="hvrbox-layer_top">
              <div class="hvrbox-text"><button>See More</button></div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
          <div class="hvrbox mt-4">
            <div class="container">
              <img class="hvrbox-layer_bottom" src="/storage/banners/blogs.jpg" style="width:100%;">
              <div class="text-block">
                <h4>Blog</h4>
              </div>
            </div>
            <div class="hvrbox-layer_top">
              <div class="hvrbox-text"><button>See More</button></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection