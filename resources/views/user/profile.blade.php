@extends('layouts.user.master') 
@section('content')
<div class="main">
  <div class="section section-basic" id="basic-elements">

    <!-- Start Here! -->

    <div class="page-header">
      <img src="../assets/img/dots.png" class="dots">
      <img src="../assets/img/path4.png" class="path">

      <br><br><br>

      <div class="col-md-10 offset-md-1">
        <a href="#"><img class="d-block img-raised img-fluid" src="/assets/img/denys2.jpg" alt="Cover Photo" style=""></a>
      </div>

      <div class="container align-items-center">
        <div class="row">
          {{-- <div class="col-md-6">
            <a href="#"><img class="d-block img-raised" src="/assets/img/denys.jpg" alt="Cover Photo"></a>
          </div> --}}
        </div>
        <div class="row">
          <div class="col-lg-4">
            <div class="card card-coin card-plain">
              <div class="card-header">
                <a href="#"><img src="/assets/img/ryan.jpg" alt="Profile Picture" class="img-raised"></a>
                <h4 class="title">PROFILE</h4>
              </div>
              <div class="card-body">
                <div class="tab-content tab-subcategories">
                  <div class="tab-pane active" id="linka">
                    <!-- <div class="table-responsive"> -->
                    <div>
                      <table class="table tablesorter " id="plain-table">
                        <thead class=" text-primary">
                          <tr>
                            <th class="header">
                              NAME:
                            </th>
                            <th class="header">
                              Lorem Ipsum
                            </th>
                          </tr>
                          <tr>
                            <th class="header">
                              EMail:
                            </th>
                            <th class="header">
                              Lorem.Ipsum@mail.com
                            </th>
                          </tr>
                          <tr>
                            <th class="header">
                              Location:
                            </th>
                            <th class="header">
                              London, United Kingdom
                            </th>
                          </tr>
                          <tr>
                            <th class="header">
                              Phone:
                            </th>
                            <th class="header">
                              +0123456789
                            </th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection