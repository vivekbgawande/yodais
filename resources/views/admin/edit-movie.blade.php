@extends('layouts.admin.master') 
@section('content')
<div class="row">
  <div class="col-sm-8 offset-sm-2">
    <div class="card">
      <div class="card-header">
        <strong>Movie / TV Series</strong>
        <small>Form</small>
      </div>
      <form action="/movies/{{$movie->id}}" method="POST" enctype="multipart/form-data">
        @csrf @method('PATCH')
  @include('layouts.errors')

        <div class="card-body">

          <label for="company">Title</label>
          <div class="form-group">
            <input class="form-control" id="company" name="title" type="text" placeholder="Enter Title" value="{{$movie->title}}">
          </div>

          <label for="textarea-input" style="float: left;">Synopsis</label>
          <div class="form-group">
            <textarea class="form-control" id="textarea-input" type="textarea" name="synopsis" rows="9" placeholder="Synopsis...">{{ $movie->synopsis }}</textarea>
          </div>

          <label for="select2">Genre</label>
          <div class="form-group">
            <select class="form-control form-control-lg" id="select2" name="genre">
              <option value="action" @if ($movie->genre=='action') {{"selected"}} @endif>Action</option>
              <option value="comedy" @if ($movie->genre=='comedy') {{"selected"}} @endif>Comedy</option>
            </select>
          </div>

          <label for="select2">Language</label>
          <div class="form-group">
            <select class="form-control form-control-lg" id="select2" name="language">
              <option value="english" @if ($movie->language=='english') {{"selected"}} @endif>English</option>
              <option value="spanish" @if ($movie->language=='spanish') {{"selected"}} @endif>Spanish</option>
              <option value="chinese" @if ($movie->language=='chinese') {{"selected"}} @endif>Chinese</option>
            </select>
          </div>

          <label for="select2">Rating</label>
          <div class="form-group">
            <select class="form-control form-control-lg" id="select2" name="rating">
              <option value="TV-Y" @if ($movie->rating=='TV-Y') {{"selected"}} @endif>TV-Y</option>
              <option value="TV-Y7" @if ($movie->rating=='TV-Y7') {{"selected"}} @endif>TV-Y7</option>
              <option value="TV-G" @if ($movie->rating=='TV-G') {{"selected"}} @endif>TV-G</option>
              <option value="TV-PG" @if ($movie->rating=='TV-PG') {{"selected"}} @endif>TV-PG</option>
              <option value="TV-14" @if ($movie->rating=='TV-14') {{"selected"}} @endif>TV-14</option>
              <option value="TV-MA" @if ($movie->rating=='TV-MA') {{"selected"}} @endif>TV-MA</option>
            </select>
          </div>

          <label for="date-input">Release Date</label>
          <div class="form-group">
            <input class="form-control" id="date-input" type="date" name="release_date" placeholder="Release Date" value="{{$movie->release_date}}">
            <span class="help-block">Please enter a valid date</span>
          </div>

          <label for="vat">Run Time</label>
          <div class="form-group">
            <input class="form-control" id="vat" type="text" placeholder="HH:MM" name="run_time" value="{{$movie->run_time}}">
          </div>

          <div class="embed-responsive embed-responsive-16by9">
            <video class="embed-responsive-item" src="{{asset($movie->movie_file)}}" controls></video>
          </div>
          <br>
          <label for="file-input">Change Movie</label>
          <div class="form-group">
            <input id="file-input" type="file" name="movie_file">
          </div>

          @if ($movie->poster_art)
          <img src="{{$movie->poster_art}}" class="img-fluid" alt="Poster"> @endif
          <br><br>
          <label for="file-input">Change Poster Art</label>
          <div class="form-group">
            <input id="file-input" type="file" name="poster_art">
          </div>


          @if ($movie->trailer_file)

          <div class="embed-responsive embed-responsive-16by9">
            <video class="embed-responsive-item" src="{{asset($movie->trailer_file)}}" controls></video>
          </div>

          @endif
          
          <br>
          <label for="file-input">Change Trailer</label>
          <div class="form-group">
            <input id="file-input" type="file" name="trailer_file">
          </div>

          <label for="company">Copyright Number</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="copyright_no" value="{{$movie->copyright_no}}">
          </div>

          <label for="company">Country of Origin</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="country" value="{{$movie->country}}">
          </div>

          <label for="company">Director(s)</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="directors" value="{{$movie->directors}}">
          </div>

          <label for="company">Producer(s)</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="producers" value="{{$movie->producers}}">
          </div>

          <label for="company">Writer(s)</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="writers" value="{{$movie->writers}}">
          </div>

          <label for="company">Cast</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="cast" value="{{$movie->cast}}">
          </div>

          <label for="company">Preview Weblink/URL</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="preview_link" value="{{$movie->preview_link}}">
          </div>

          <label for="company">Vendor Item ID</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="vendor_id" value="{{$movie->vendor_id}}">
          </div>

          <label for="company">Image Weblink/URL</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="image_link" value="{{$movie->image_link}}">
          </div>

          <label for="company">Alternate Title(s)</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="alternate_titles" value="{{$movie->alternate_titles}}">
          </div>
          <br>
          <button class="btn btn-sm btn-success" type="submit">
                    <i class="fa fa-dot-circle-o"></i> Submit</button>
          <button class="btn btn-sm btn-danger" type="reset">
                    <i class="fa fa-ban"></i> Reset</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection