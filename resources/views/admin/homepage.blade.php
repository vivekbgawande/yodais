@extends('layouts.admin.master') 
@section('content')
<div class="row">
  <div class="col-lg-8 offset-lg-2">
    <div class="card">
      <div class="card-header">
        <i class="fa fa-align-justify"></i><strong>Homepage Banners</strong> </div>
      <div class="card-body">
        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
          <div class="card-header">
            <div class="col-lg-5 offset-lg-5">
              <div class="form-group row"><b>&nbspMovie/TV</b></div>
            </div>
          </div><br>
          <div class="form-group row">
            <img class="img-fluid mx-auto" src="/img/sample-image.png">
          </div>

          <div class="form-group row">
            <div class="mx-auto">
              Banner&nbsp;&nbsp;<input id="file-multiple-input" type="file" name="file-multiple-input" multiple="">
            </div>
          </div>
          <div class="card-header">
            <div class="col-lg-5 offset-lg-5">
              <div class="form-group row"><b>&nbsp&nbsp&nbspPhotos </b></div>
            </div>
          </div><br>
          <div class="form-group row">
            <img class="img-fluid mx-auto" src="/img/sample-image.png">
          </div>
          <div class="form-group row">
            <div class="mx-auto">
              Banner&nbsp;&nbsp;<input id="file-multiple-input" type="file" name="file-multiple-input" multiple="">
            </div>
          </div>
          <div class="card-header">
            <div class="col-lg-5 offset-lg-5">
              <div class="form-group row"><b>Digital Artworks</b></div>
            </div>
          </div><br>
          <div class="form-group row">
            <img class="img-fluid mx-auto" src="/img/sample-image.png">
          </div>
          <div class="form-group row">
            <div class="mx-auto">
              Banner&nbsp;&nbsp;<input id="file-multiple-input" type="file" name="file-multiple-input" multiple="">
            </div>
          </div>
          <div class="card-header">
            <div class="col-lg-5 offset-lg-5">
              <div class="form-group row"><b>&nbspMusic</b></div>
            </div>
          </div><br>
          <div class="form-group row">
            <img class="img-fluid mx-auto" src="/img/sample-image.png">
          </div>
          <div class="form-group row">
            <div class="mx-auto">
              Banner&nbsp;&nbsp;<input id="file-multiple-input" type="file" name="file-multiple-input" multiple="">
            </div>
          </div>
          <div class="card-header">
            <div class="col-lg-5 offset-lg-5">
              <div class="form-group row"><b>&nbspBlog</b></div>
            </div>
          </div><br>
          <div class="form-group row">
            <img class="img-fluid mx-auto" src="/img/sample-image.png">
          </div>
          <div class="form-group row">
            <div class="mx-auto">
              Banner&nbsp;&nbsp;<input id="file-multiple-input" type="file" name="file-multiple-input" multiple="">
            </div>
          </div>
          <div class="card-header">
            <div class="col-lg-5 offset-lg-5">
              <div class="form-group row"><b>Subscribed</b></div>
            </div>
          </div><br>
          <div class="form-group row">
            <img class="img-fluid mx-auto" src="/img/sample-image.png">
          </div>
          <div class="form-group row">
            <div class="mx-auto">
              Banner&nbsp;&nbsp;<input id="file-multiple-input" type="file" name="file-multiple-input" multiple="">
            </div>
          </div>
      </div>
      
      <div class="card-footer">
        <button class="btn btn-sm btn-primary" type="submit">
                      <i class="fa fa-dot-circle-o"></i> Submit</button>
        <button class="btn btn-sm btn-danger" type="reset">
                      <i class="fa fa-ban"></i> Reset</button>
      </div>
    </div>
  </div>
@endsection