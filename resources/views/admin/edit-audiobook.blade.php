@extends('layouts.admin.master') 
@section('content')
<div class="row">
  <div class="col-sm-8 offset-sm-2">
    <div class="card">
      <div class="card-header">
        <strong>Audio Book Edit</strong>
        <small>Form</small>
      </div>
      <div class="card-body">
  @include('layouts.errors')
        <form action="/admin/audiobooks/{{ $audioBook->id }}" method="post" enctype="multipart/form-data">
          @csrf @method('patch')
          <label for="title">Book Title</label>
          <div class="form-group">
            <input class="form-control" name="title" id="title" type="text" value="{{ $audioBook->title }}" placeholder="Enter book title">
          </div>

          <label for="author">Author</label>
          <div class="form-group">
            <input class="form-control" name="author" id="author" type="text" value="{{ $audioBook->author }}" placeholder="Enter author's name">
          </div>

          <label for="narrator">Narrated By</label>
          <div class="form-group">
            <input class="form-control" id="narrator" name="narrator" value="{{ $audioBook->narrator }}" type="text" placeholder="Enter narrator's name">
          </div>

          <label for="genre">Genre</label>
          <div class="form-group">
            <select class="form-control form-control-lg" id="genre" name="genre">
              <option value="action" @if ($audioBook->genre=='action') {{"selected"}} @endif>Action</option>
              <option value="comedy" @if ($audioBook->genre=='comedy') {{"selected"}} @endif>Comedy</option>
            </select>
          </div>

          <label for="language">Language</label>
          <div class="form-group">
            <select class="form-control form-control-lg" id="select2" name="language">
                  <option value="english" @if ($audioBook->language=='english') {{"selected"}} @endif>English</option>
                  <option value="spanish" @if ($audioBook->language=='spanish') {{"selected"}} @endif>Spanish</option>
                  <option value="chinese" @if ($audioBook->language=='chinese') {{"selected"}} @endif>Chinese</option>
                </select>
          </div>

            <div class="form-group">
              <label for="release_date">Release Date</label>
              <input class="form-control" id="release_date" type="date" name="release_date" value="{{ $audioBook->release_date }}" placeholder="Release Date">
              <span class="help-block">Please enter a valid date</span>
            </div>
          </div>

          <label for="song"> Current Song</label>
          <div class="embed-responsive pt-5">
            <audio class="embed-responsive-item" src="{{asset($audioBook->audio_file)}}" controls></audio>
          </div>

          <label for="audio_file">Change Song</label>
          <div class="form-group">
            <input id="audio_file" type="file" name="audio_file">
            <span class="help-block">File formats supported are: .mp3, .wav</span>
          </div>

          <div class="form-group">
            <label for="copyright_no">Copyright Number</label>
            <input class="form-control" id="copyright_no" name="copyright_no" value="{{ $audioBook->copyright_no }}" type="number" placeholder="Enter copyright number">
          </div>

          <div class="form-group">
            <label for="country">Country of Origin</label>
            <input class="form-control" id="country" name="country" type="text" value="{{ $audioBook->country }}" placeholder="Enter country of origin">
          </div>

          <div class="form-group">
            <label for="isbn">ISBN Number</label>
            <input class="form-control" id="isbn" name="isbn" type="text" value="{{ $audioBook->isbn }}" placeholder="Enter ISBN number">
          </div>

          <div class="form-group">
            <label for="publisher">Publisher</label>
            <input class="form-control" id="publisher" name="publisher" type="text" value="{{ $audioBook->publisher }}" placeholder="Enter publisher's name">
          </div>

          <div class="form-group">
            <label for="synopsis" style="float: left;">Synopsis</label>
            <textarea class="form-control" id="synopsis" type="textarea" name="synopsis" rows="9" placeholder="Brief Description...">{{ $audioBook->synopsis }}</textarea>
          </div>

          <br>

          <button class="btn btn-sm btn-success" type="submit">
                        <i class="fa fa-dot-circle-o"></i> Submit</button>
          <button class="btn btn-sm btn-danger" type="reset">
                        <i class="fa fa-ban"></i> Reset</button>

        </form>
      </div>
    </div>
  </div>
</div>
@endsection