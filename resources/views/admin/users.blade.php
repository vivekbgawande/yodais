@extends('layouts.admin.master') 

@section('content')
<div class="row">
    <div class="col-lg-10 offset-lg-1">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> All Users</div>
            <div class="card-body">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>UserID</th>
                            <th>Name</th>
                            <th>EMail</th>
                            <th>Locations</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>U1</td>
                            <td>User 1</td>
                            <td>user1@xyz.com</td>
                            <td>L1</td>
                            <td>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="#"><button class="btn-ghost-success" type="button"><i class="icons font-2xl d-block cui-pencil"></i></button></a>
                                    </div>
                                    <div class="col-sm-6">
                                        <form action="#" method="post">
                                            @csrf @method('delete')
                                            <button class="btn-ghost-danger" type="submit"><i class="icons font-2xl d-block cui-trash"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>U2</td>
                            <td>User 2</td>
                            <td>user2@xyz.com</td>
                            <td>L2</td>
                            <td>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="#"><button class="btn-ghost-success" type="button"><i class="icons font-2xl d-block cui-pencil"></i></button></a>
                                    </div>
                                    <div class="col-sm-6">
                                        <form action="#" method="post">
                                            @csrf @method('delete')
                                            <button class="btn-ghost-danger" type="submit"><i class="icons font-2xl d-block cui-trash"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>U3</td>
                            <td>User 3</td>
                            <td>user3@xyz.com</td>
                            <td>L3</td>
                            <td>
                                                              
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="#"><button class="btn-ghost-success" type="button"><i class="icons font-2xl d-block cui-pencil"></i></button></a>
                                    </div>
                                    <div class="col-sm-6">
                                        <form action="#" method="post">
                                            @csrf @method('delete')
                                            <button class="btn-ghost-danger" type="submit"><i class="icons font-2xl d-block cui-trash"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>U4</td>
                            <td>User 4</td>
                            <td>user4@xyz.com</td>
                            <td>L4</td>
                            <td>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="#"><button class="btn-ghost-success" type="button"><i class="icons font-2xl d-block cui-pencil"></i></button></a>
                                    </div>
                                    <div class="col-sm-6">
                                        <form action="#" method="post">
                                            @csrf @method('delete')
                                            <button class="btn-ghost-danger" type="submit"><i class="icons font-2xl d-block cui-trash"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>U5</td>
                            <td>User 5</td>
                            <td>user5@xyz.com</td>
                            <td>L5</td>
                            <td>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="#"><button class="btn-ghost-success" type="button"><i class="icons font-2xl d-block cui-pencil"></i></button></a>
                                    </div>
                                    <div class="col-sm-6">
                                        <form action="#" method="post">
                                            @csrf @method('delete')
                                            <button class="btn-ghost-danger" type="submit"><i class="icons font-2xl d-block cui-trash"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link" href="#">Prev</a>
                    </li>
                    <li class="page-item active">
                        <a class="page-link" href="#">1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">4</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection