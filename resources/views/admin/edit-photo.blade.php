@extends('layouts.admin.master') 
@section('content')
<div class="row">
  <div class="col-sm-8 offset-sm-2">
    <div class="card">
      <div class="card-header">
        <strong>Photo Registration</strong>
        <small>Form</small>
      </div>
      <div class="card-body">
        <form action="/admin/photos/{{ $photo->id }}" method="post" enctype="multipart/form-data">
          @csrf @method('patch')
          @include('layouts.errors')
          
          <label for="title"> Photo Title</label>
          <div class="form-group">
            <input name="title" class="form-control" id="title" type="text" value="{{ $photo->title }}">
          </div>

          <label for="description" style="float:left;">Description</label>
          <div class="form-group">
            <textarea class="form-control" id="description" type="textarea" name="description" rows="9">{{ $photo->description }}</textarea>
          </div>

          <div class="row">
            <div class="form-group col-sm-6">
              <label for="creation_date">Date of Creation</label>
              <input class="form-control" id="creation_date" type="date" name="creation_date" value="{{ $photo->creation_date }}">              {{-- <span class="help-block">Please enter a valid date</span> --}}
            </div>

            <div class="form-group col-sm-6">
              <label for="location">Location</label>
              <input class="form-control" name="location" id="location" type="text" value="{{ $photo->location }}">
            </div>
          </div>

          <img src="{{ $photo->file }}" alt="{{ $photo->name }}" class="img-fluid"><br><br>
          <label for="file">File</label>
          <div class="form-group">
            <input id="file" type="file" name="file">
          </div>

          <label for="copyright_no">Copyright Number</label>
          <div class="form-group">
            <input class="form-control" name="copyright_no" id="copyright_no" type="number" value="{{ $photo->copyright_no }}">
          </div>
          <br>
          <input class="btn btn-sm btn-success" type="submit">
          <input class="btn btn-sm btn-danger" type="reset" value="Reset">
        </form>
      </div>
    </div>
  </div>
</div>
@endsection