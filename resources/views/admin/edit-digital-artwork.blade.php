@extends('layouts.admin.master') 
@section('content')
<div class="row">
  <div class="col-md-8 offset-md-2">
    <div class="card">
      <div class="card-header">
        <strong>Digital Artwork Registration</strong>
        <small>Form</small>
      </div>
      <div class="card-body">
        <form action="/admin/digital-artworks/{{ $digitalArtwork->id }}" method="post" enctype="multipart/form-data">
          @csrf @method('patch')
  @include('layouts.errors')
          <label for="title"> Digital Artwork Title</label>
          <div class="form-group">
            <input class="form-control" name="title" id="title" type="text" value="{{ $digitalArtwork->title }}">
          </div>

          <label for="description" style="float: left;">Description</label>
          <div class="form-group">
            <textarea class="form-control" id="description" type="textarea" name="description" rows="9">{{ $digitalArtwork->description }}</textarea>
          </div>

          <label for="creation_date">Date of Creation</label>
          <div class="form-group">
            <input class="form-control" id="creation_date" type="date" name="creation_date" value="{{ $digitalArtwork->creation_date }}">
          </div>

          <label for="location"> Location</label>
          <div class="form-group">
            <input class="form-control" id="location" type="text" name="location" value="{{ $digitalArtwork->location }}">
          </div>
          <img src="{{ $digitalArtwork->file }}" alt="{{ $digitalArtwork->name }}" class="img-fluid"><br><br>
          <label for="file">File</label>
          <div class="form-group">
            <input id="file" type="file" name="file">
          </div>

          <label for="copyright_no">Copyright Number</label>
          <div class="form-group">
            <input class="form-control" id="copyright_no" name="copyright_no" type="number" value="{{ $digitalArtwork->copyright_no }}">
          </div>
          <br>
          <button class="btn btn-sm btn-success" type="submit">
                        <i class="fa fa-dot-circle-o"></i> Submit</button>
          <button class="btn btn-sm btn-danger" type="reset">
                        <i class="fa fa-ban"></i> Reset</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection