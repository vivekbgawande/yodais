@extends('layouts.admin.master') 
@section('content')
<div class="row">
  <div class="col-lg-8 offset-lg-2">
    <div class="card">
      <div class="card-header">
        <i class="fa fa-align-justify"></i><strong>Homepage Banners</strong> </div>
      <div class="card-body">
        <form class="form-horizontal" action="/admin/homepage" method="post" enctype="multipart/form-data">
          @csrf
  @include('layouts.errors')

          <div class="card-header">
            <div class="col-lg-5 offset-lg-5">
              <div class="form-group row"><b>&nbspMovie/TV</b></div>
            </div>
          </div><br>

          <img class="img-fluid" src="/storage/banners/movies.jpg">
          <br><br>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="movies_image"> Banner:</label>
            <div class="col-md-9">
              <input id="movies_image" type="file" name="movies_image">
            </div>
          </div>

          <div class="card-header">
            <div class="col-lg-5 offset-lg-5">
              <div class="form-group row"><b>&nbsp&nbsp&nbspPhotos </b></div>
            </div>
          </div><br>

          <img class="img-fluid" src="/storage/banners/photos.jpg">
          <br><br>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="photos_image"> Banner :</label>
            <div class="col-md-9">
              <input id="photos_image" type="file" name="photos_image">
            </div>
          </div>

          <div class="card-header">
            <div class="col-lg-5 offset-lg-5">
              <div class="form-group row"><b>Digital Artworks</b></div>
            </div>
          </div><br>

          <img class="img-fluid" src="/storage/banners/digital-artworks.jpg">
          <br><br>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="artworks_image"> Banner :</label>
            <div class="col-md-9">
              <input id="artworks_image" type="file" name="artworks_image">
            </div>
          </div>

          <div class="card-header">
            <div class="col-lg-5 offset-lg-5">
              <div class="form-group row"><b>&nbspMusic</b></div>
            </div>
          </div><br>

          <img class="img-fluid" src="/storage/banners/musics.jpg">
          <br><br>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="musics_image"> Banner :</label>
            <div class="col-md-9">
              <input id="musics_image" type="file" name="musics_image">
            </div>
          </div>

          <div class="card-header">
            <div class="col-lg-5 offset-lg-5">
              <div class="form-group row"><b>&nbspBlog</b></div>
            </div>
          </div><br>

          <img class="img-fluid" src="/storage/banners/blogs.jpg">
          <br><br>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="blogs_image"> Banner :</label>
            <div class="col-md-9">
              <input id="blogs_image" type="file" name="blogs_image">
            </div>
          </div>

          <div class="card-header">
            <div class="col-lg-5 offset-lg-5">
              <div class="form-group row"><b>Subscribed</b></div>
            </div>
          </div><br>

          <img class="img-fluid" src="/storage/banners/subscribed.jpg">
          <br><br>
          <div class="form-group row">
            <label class="col-md-3 col-form-label" for="subscribed_image"> Banner :</label>
            <div class="col-md-3">
              <input id="subscribed_image" type="file" name="subscribed_image">
            </div>
          </div>
      </div>
      <div class="card-footer">
        <button class="btn btn-sm btn-primary" type="submit">
                      <i class="fa fa-dot-circle-o"></i> Submit</button>
        <button class="btn btn-sm btn-danger" type="reset">
                      <i class="fa fa-ban"></i> Reset</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection