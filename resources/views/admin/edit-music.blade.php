@extends('layouts.admin.master') 
@section('content')
<div class="row">
  <div class="col-sm-8 offset-sm-2">
    <div class="card">
      <div class="card-header">
        <strong>Music Modification</strong>
        <small>Form</small>
      </div>
      <div class="card-body">
        @include('layouts.errors')
        <form action="/admin/music/{{ $music->id }}" method="post" enctype="multipart/form-data">
          @csrf @method('patch')

          <label for="artist"> Artist</label>
          <div class="form-group">
            <input class="form-control" id="artist" name="artist" type="text" placeholder="Enter artist's name" value="{{ $music->artist }}">
          </div>

          <label for="title"> Song Title</label>
          <div class="form-group">
            <input class="form-control" id="title" name="title" type="text" value="{{ $music->title }}" placeholder="Enter song title">
          </div>

          <label for="album_name"> Album</label>
          <div class="form-group">
            <input class="form-control" id="album_name" name="album_name" type="text" value="{{ $music->album_name }}" placeholder="Enter album name">
          </div>

          <label for="genre">Genre</label>
          <div class="form-group">
            <select class="form-control form-control-lg" id="genre" name="genre">
              <option value="action" @if ($music->genre=='action') {{"selected"}} @endif>Action</option>
              <option value="comedy" @if ($music->genre=='comedy') {{"selected"}} @endif>Comedy</option>
            </select>
          </div>

          <label for="language">Language</label>
          <div class="form-group">
            <select class="form-control form-control-lg" id="select2" name="language">
                  <option value="english" @if ($music->language=='english') {{"selected"}} @endif>English</option>
                  <option value="spanish" @if ($music->language=='spanish') {{"selected"}} @endif>Spanish</option>
                  <option value="chinese" @if ($music->language=='chinese') {{"selected"}} @endif>Chinese</option>
                </select>
          </div>

          <div class="row">
            <div class="form-group col-sm-6">
              <label for="rating">Rating</label>
              <select class="form-control form-control-lg" id="select2" name="rating">
                    <option value="TV-Y" @if ($music->rating=='TV-Y') {{"selected"}} @endif>TV-Y</option>
                    <option value="TV-Y7" @if ($music->rating=='TV-Y7') {{"selected"}} @endif>TV-Y7</option>
                    <option value="TV-G" @if ($music->rating=='TV-G') {{"selected"}} @endif>TV-G</option>
                    <option value="TV-PG" @if ($music->rating=='TV-PG') {{"selected"}} @endif>TV-PG</option>
                    <option value="TV-14" @if ($music->rating=='TV-14') {{"selected"}} @endif>TV-14</option>
                    <option value="TV-MA" @if ($music->rating=='TV-MA') {{"selected"}} @endif>TV-MA</option>
                  </select>
              </select>
            </div>

            <div class="form-group col-sm-6">
              <label for="release_date">Release Date</label>
              <input class="form-control" id="release_date" type="date" name="release_date" value="{{ $music->release_date }}" placeholder="Release Date">
            </div>
          </div>

          <label for="song"> Current Song</label>
          <div class="embed-responsive pt-5">
            <audio class="embed-responsive-item" src="{{asset($music->audio_file)}}" controls></audio>
          </div>

          <label for="audio_file">Change Song</label>
          <div class="form-group">
            <input id="audio_file" type="file" name="audio_file">
            <span class="help-block">File formats supported are: .mp3, .wav</span>
          </div>

          @if ($music->album_art)
            <img src="{{$music->album_art}}" class="img-fluid" alt="Poster"> 
          @endif
          <br><br>

          <label for="album_art">Change Album Art</label>
          <div class="form-group">
            <input id="album_art" type="file" name="album_art">
          </div>

          <label for="copyright_no">Copyright Number</label>
          <div class="form-group">
            <input class="form-control" name="copyright_no" id="copyright_no" type="number" value="{{ $music->copyright_no }}" placeholder="Enter copyright number">
          </div>

          <label for="country">Country of Origin</label>
          <div class="form-group">
            <input class="form-control" name="country" id="country" type="text" value="{{ $music->country }}" placeholder="Enter country of origin">
          </div>

          <label for="songwriters" style="float:left;">SongWriter(s)</label>
          <div class="form-group">
            <input class="form-control" id="songwriters" name="songwriters" type="text" value="{{ $music->songwriters }}" placeholder="Enter song writer's name">
          </div>

          <label for="commposers" style="float: left;">Composer(s)</label>
          <div class="form-group">
            <input class="form-control" id="composers" name="composers" type="text" value="{{ $music->composers }}" placeholder="Enter composer's name">
          </div>

          <label for="isrc"> ISRC</label>
          <div class="form-group">
            <input class="form-control" id="isrc" name="isrc" value="{{ $music->isrc }}" type="number" placeholder="Enter ISRC">
          </div>

          <label for="iswc"> ISWC</label>
          <div class="form-group">
            <input class="form-control" id="iswc" name="iswc" type="text" value="{{ $music->iswc }}" placeholder="Enter ISWC">
          </div>

          <label for="recording_label"> Recording Label</label>
          <div class="form-group">
            <input class="form-control" id="recording_label" name="recording_label" value="{{ $music->recording_label }}" type="text"
              placeholder="Enter recording label">
          </div>

          <label for="publisher"> Music publisher(s)</label>
          <div class="form-group">
            <input class="form-control" id="publisher" name="publisher" type="text" value="{{ $music->publisher }}" placeholder="Enter music publisher's name">
          </div>
          <br>
          <button class="btn btn-sm btn-success" type="submit">
                      <i class="fa fa-dot-circle-o"></i> Submit</button>
          <button class="btn btn-sm btn-danger" type="reset">
                      <i class="fa fa-ban"></i> Reset</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection