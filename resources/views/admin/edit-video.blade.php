@extends('layouts.admin.master') 
@section('content')
<div class="row">
  <div class="col-sm-8 offset-sm-2">
    <div class="card">
      <div class="card-header">
        <strong>Edit Video Clips</strong>
        <small>Form</small>
      </div>
      <form action="/admin/videos/{{$video->id}}" method="post" enctype="multipart/form-data">
        @csrf @method('PATCH')
        @include('layouts.errors')

        <div class="card-body">
          <label for="company">Title</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" placeholder="Enter Title" name="title" value="{{$video->title}}">
          </div>

          <label for="company">Creator(s)</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" placeholder="Enter Creator" name="creator" value="{{$video->creator}}">
          </div>

          <label for="textarea-input" style="float: left;">Description</label>
          <div class="form-group">
            <textarea class="form-control" id="textarea-input" type="textarea" name="description" rows="9" placeholder="Brief Description...">{{$video->description}}</textarea>
          </div>

          <div class="row">
            <div class="form-group col-sm-6">
              <label for="select2">Language</label>
              <select class="form-control form-control-lg" id="select2" name="language">
                <option value="english" @if ($video->language=='english') {{"selected"}} @endif>English</option>
                <option value="spanish" @if ($video->language=='spanish') {{"selected"}} @endif>Spanish</option>
                <option value="chinese" @if ($video->language=='chinese') {{"selected"}} @endif>Chinese</option>
              </select>
            </div>

            <div class="form-group col-sm-6">
              <label for="date-input">Date of Creation</label>
              <input class="form-control" id="date-input" type="date" name="creation_date" placeholder="Creation Date" value="{{$video->creation_date}}">
            </div>
          </div>

          @if ($video->video_file)

          <div class="embed-responsive embed-responsive-16by9">
            <video class="embed-responsive-item" src="{{asset($video->video_file)}}" controls></video>
          </div>

          @endif

          <label for="file-input">Change Video Clip</label>
          <div class="form-group">
            <input id="file-input" type="file" name="video_file">
          </div>

          <label for="company">Copyright Number</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" placeholder="Enter copyright number" name="copyright_no" value="{{$video->copyright_no}}">
          </div>

          <label for="company">Director(s)</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" placeholder="Enter director's name" name="directors" value="{{$video->directors}}">
          </div>

          <label for="company">Producer(s)</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" placeholder="Enter producer's name" name="producers" value="{{$video->producers}}">
          </div>

          <label for="company">Writer(s)</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" placeholder="Enter writer's name" name="writers" value="{{$video->writers}}">
          </div>

          <label for="company">Cast</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" placeholder="Enter cast members" name="cast" value="{{$video->cast}}">
          </div>

          <label for="company">Preview Weblink/URL</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="preview_link" value="{{$video->preview_link}}">
          </div>

          <label for="company">Vendor Item ID</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" placeholder="Enter item ID" name="vendor_id" value="{{$video->vendor_id}}">
          </div>

          <label for="company">Image Weblink/URL</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="image_link" value="{{$video->image_link}}">
          </div>

          <label for="company">Alternate Title(s)</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" placeholder="Enter alternate title" name="alternate_titles" value="{{$video->alternate_titles}}">
          </div>
          <br>

          <button class="btn btn-sm btn-success" type="submit">
                      <i class="fa fa-dot-circle-o"></i> Submit</button>
          <button class="btn btn-sm btn-danger" type="reset">
                      <i class="fa fa-ban"></i> Reset</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection