@extends('layouts.admin.master') 
@section('content')
<div class="row">
    <div class="col-lg-10 offset-lg-1">
        <div class="card">
            <div class="card-header">
                <i class="fa fa-align-justify"></i> Movie/TV Table</div>
            <div class="card-body">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Rating</th>
                            <th>Release Date</th>
                            <th>Options</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($movies as $movie)

                        <tr>
                            <td>{{ $movie->title }}</td>
                            <td>{{ $movie->rating }}</td>
                            <td>{{ $movie->release_date }}</td>
                            <td>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="/admin/movies/{{$movie->id}}/edit"><button class="btn-ghost-success" type="button"><i class="icons font-2xl d-block cui-pencil"></i></button></a>
                                    </div>
                                    <div class="col-sm-6">
                                        <form action="/admin/movies/{{ $movie->id }}" method="post">
                                            @csrf @method('delete')
                                            <button class="btn-ghost-danger" type="submit"><i class="icons font-2xl d-block cui-trash"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>

                        @endforeach

                    </tbody>
                </table>
                <ul class="pagination">
                    <li class="page-item">
                        <a class="page-link" href="#">Prev</a>
                    </li>
                    <li class="page-item active">
                        <a class="page-link" href="#">1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">3</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">4</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection