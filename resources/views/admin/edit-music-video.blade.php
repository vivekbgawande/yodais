@extends('layouts.admin.master') 
@section('content')
<div class="row">
  <div class="col-sm-8 offset-sm-2">
    <div class="card">
      <div class="card-header">
        <strong>Edit Music Videos</strong>
        <small>Form</small>
      </div>
      <form action="/music-videos/{{$musicVideo->id}}" method="post" enctype="multipart/form-data">
        @csrf @method('PATCH')
        @include('layouts.errors')
        
        <div class="card-body">
          <label for="company">Song Name</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="song" placeholder="Enter Song Name" value="{{ $musicVideo->song }}">
          </div>

          <label for="company">Artist Name</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="artist" placeholder="Enter Artist Name" value="{{ $musicVideo->artist }}">
          </div>

          <label for="company">Album Name</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="album" placeholder="Enter Album Name" value="{{ $musicVideo->album }}">
          </div>

          <label for="select2">Genre</label>
          <div class="form-group">
            <select class="form-control form-control-lg" id="select2" name="genre">
              <option value="action" @if ($musicVideo->genre=='action') {{"selected"}} @endif>Action</option>
              <option value="comedy" @if ($musicVideo->genre=='comedy') {{"selected"}} @endif>Comedy</option>
            </select>
          </div>

          <label for="select2">Language</label>
          <div class="form-group">
            <select class="form-control form-control-lg" id="select2" name="language">
            <option value="english" @if ($musicVideo->language=='english') {{"selected"}} @endif>English</option>
            <option value="spanish" @if ($musicVideo->language=='spanish') {{"selected"}} @endif>Spanish</option>
            <option value="chinese" @if ($musicVideo->language=='chinese') {{"selected"}} @endif>Chinese</option>
          </select>
          </div>

          <div class="row">
            <div class="form-group col-sm-6">

              <label for="select2">Rating</label>
              <select class="form-control form-control-lg" id="select2" name="rating">
              <option value="TV-Y" @if ($musicVideo->rating=='TV-Y') {{"selected"}} @endif>TV-Y</option>
              <option value="TV-Y7" @if ($musicVideo->rating=='TV-Y7') {{"selected"}} @endif>TV-Y7</option>
              <option value="TV-G" @if ($musicVideo->rating=='TV-G') {{"selected"}} @endif>TV-G</option>
              <option value="TV-PG" @if ($musicVideo->rating=='TV-PG') {{"selected"}} @endif>TV-PG</option>
              <option value="TV-14" @if ($musicVideo->rating=='TV-14') {{"selected"}} @endif>TV-14</option>
              <option value="TV-MA" @if ($musicVideo->rating=='TV-MA') {{"selected"}} @endif>TV-MA</option>
            </select>
            </div>

            <div class="form-group col-sm-6">
              <label for="date-input">Release Date</label>
              <input class="form-control" id="date-input" type="date" name="release_date" placeholder="Release Date" value="{{ $musicVideo->release_date }}">
              <span class="help-block">Please enter a valid date</span>
            </div>

          </div>


          @if ($musicVideo->song_file)

          <div class="embed-responsive embed-responsive-16by9">
            <video class="embed-responsive-item" src="{{asset($musicVideo->song_file)}}" controls></video>
          </div>

          @endif

          <br>
          <label for="file-input">Music Video</label>
          <div class="form-group">
            <input id="file-input" type="file" name="song_file">
          </div>

          @if ($musicVideo->album_art)
          <img src="{{$musicVideo->album_art}}" class="img-fluid" alt="Poster"> @endif
          <br><br>

          <label for="file-input">Album Art</label>
          <div class="form-group">
            <input id="file-input" type="file" name="album_art">
          </div>


          <label for="company">Copyright Number</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="copyright_no" placeholder="Enter copyright number" value="{{ $musicVideo->copyright_no }}">
          </div>

          <label for="company">Country of Origin</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="country" placeholder="Enter country of origin" value="{{ $musicVideo->country }}">
          </div>

          <label for="company">Director(s)</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="directors" placeholder="Enter director's name" value="{{ $musicVideo->directors }}">
          </div>

          <label for="company">Producer(s)</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="producers" placeholder="Enter producer's name" value="{{ $musicVideo->producers }}">
          </div>

          <label for="company">Writer(s)</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="writers" placeholder="Enter writer's name" value="{{ $musicVideo->writers }}">
          </div>

          <label for="company">Cast</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="cast" placeholder="Enter cast members" value="{{ $musicVideo->cast }}">
          </div>

          <label for="company">Preview Weblink/URL</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="preview_link" placeholder="Enter preview URL" value="{{ $musicVideo->preview_link }}">
          </div>

          <label for="company">Vendor Item ID</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="vendor_id" placeholder="Enter item ID" value="{{ $musicVideo->vendor_id }}">
          </div>

          <label for="company">Image Weblink/URL</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="image_link" placeholder="Enter image link" value="{{ $musicVideo->image_link }}">
          </div>

          <label for="company">Alternate Title(s)</label>
          <div class="form-group">
            <input class="form-control" id="company" type="text" name="alternate_titles" placeholder="Enter alternate title" value="{{ $musicVideo->alternate_titles }}">
          </div>

          <br>
          <button class="btn btn-sm btn-success" type="submit">
                      <i class="fa fa-dot-circle-o"></i> Submit</button>
          <button class="btn btn-sm btn-danger" type="reset">
                      <i class="fa fa-ban"></i> Reset</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection